print("IMPORT")

import bpy
import sys

filepath = sys.argv[(sys.argv.index("--directory") + 1)] + "/model.obj"

#clear scene
bpy.ops.wm.read_factory_settings(use_empty=True)

bpy.ops.import_scene.obj(filepath=filepath)
