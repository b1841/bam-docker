#!/usr/bin/env bash
# this is the (leading) path to textures in exported materials.mtl
# INSTANCE_MOD_PATH=/usr/local/share/minetest/games/unej/mods

# this is where we actually want to get textures from
# LOCAL_MOD_PATH=/Users/me/iri/clones/unej_game_recurse/mods

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

display_usage() {
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-v] [--no-colors] [-o|--ouput <output_directory>] <meshport directory>

Available options:

-h, --help      Print this help and exit
-v, --verbose   Print script debug info
--no-colors     Print message without colors
-o, --output    Soecify the <output_directory>

Parameters:

<meshport export directory> meshport directory (required)

EOF

}

usage() {
  display_usage
  exit
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # script cleanup here
  [[ -n "$workdir_tmp_dir" ]] && [[ -d "$workdir_tmp_dir" ]] && \
    rm -r "$workdir_tmp_dir"
}

setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

msg() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

parse_params() {
  # default values of variables set from params
  flag=0
  param=''

  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) set -x ;;
    --no-color) NO_COLOR=1 ;;
    -o | --output) # example named parameter
      output_dir="${2-}"
      shift
      ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
    shift
  done

  args=("$@")

  # check required params and arguments
  [[ ${#args[@]} -ne 1 ]] && display_usage && die "Bad number of arguments (1)" 2
  [[ -n "${output_dir}" ]] && [[ ! -d "${output_dir}" ]] && display_usage && die "Output dir must exists" 3

  return 0
}

workdir_tmp_dir=""
output_dir=""

parse_params "$@"
setup_colors

# script logic here

src_dir=$(realpath ${args[0]})
workdir_tmp_dir=$(mktemp -d)
if [[ -z "${output_dir}" ]]; then
    output_dir="$(pwd)"
fi
output_file="${output_dir}/$(basename $src_dir)"
cp -r "$src_dir"/* "$workdir_tmp_dir"

while read -r line
do
    if [[ $line =~ "map_Kd" ]]
    then
        filepath=$(echo $line | sed "s:map_Kd $INSTANCE_MOD_PATH\(.*\):\1:")
        dest=$workdir_tmp_dir/textures$(dirname $filepath)
        filename=$(basename $filepath)
        mkdir -p $dest
        convert "$LOCAL_MOD_PATH$filepath" -set colorspace RGB "$dest/$filename"
    fi
done < $workdir_tmp_dir/materials.mtl
#
#
sed -i "s:$INSTANCE_MOD_PATH:$workdir_tmp_dir/textures:g" $workdir_tmp_dir/materials.mtl

blender -b -P /usr/local/lib/blender/import_obj.py -P /usr/local/lib/blender/materials.py -P /usr/local/lib/blender/export_gltf.py -- --directory $workdir_tmp_dir --output $output_file
