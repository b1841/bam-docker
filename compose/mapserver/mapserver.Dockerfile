FROM minetestmapserver/mapserver:latest

ENV MAPSERVER_KEY=
ENV PROMETHEUS_ENABLED=

RUN apk --no-cache add bash jq moreutils
COPY ./mapserver_entrypoint.sh /sbin/mapserver_entrypoint.sh
RUN chmod +x /sbin/mapserver_entrypoint.sh

ENTRYPOINT [ "/sbin/mapserver_entrypoint.sh" ]

EXPOSE 8080
CMD ["/bin/mapserver"]