# Docker-compose for a local server

## commands

  1. copy `.env.tmpl` to `.env`
  2. create the containers with `docker-compose up --no-start`
  3. Start database: `docker-compose start postgres`
  4. Start minetest: `docker-compose start minetest`
  5. Stop minetest: `docker-compose stop minetest`
  6. The minetest world can be found in `data/minetest/world`
  7. Migrate backend to postgres `docker-compose run -e CLI_ARGS="--migrate postgresql" minetest`
  8. Migrate auth backend to postgres `docker-compose run -e CLI_ARGS="--migrate-auth postgresql" minetest`
  9. Migrate players backend to postgres `docker-compose run -e CLI_ARGS="--migrate-players postgresql" minetest`
  10. Start mapserver `docker-compose start mapserver`. A `mapserver.json` file is found in the world folder. The `.env` `MAPSERVER_KEY` variable must match the `webapi.secretkey` key in the json file.
  11. Start all in background `docker-compose up -d`

