ARG PUID="1000"
ARG GUID="1000"
ARG BUILD_DATE
ARG VERSION


FROM alpine:3.12

# ENV MINETEST_GAME_VERSION master
ARG MINETEST_RELEASE
ARG MINETEST_RELEASE_DEFAULT="5.4.0"
ARG UNEJ_GAME_RELEASE="master"
ARG LDFLAGS="-lintl"
ARG BUILD_DATE
ARG VERSION


WORKDIR /usr/src/minetest

RUN apk add --no-cache grep git build-base irrlicht-dev cmake bzip2-dev libpng-dev \
		jpeg-dev libxxf86vm-dev mesa-dev sqlite-dev libogg-dev libintl gettext-dev gettext \
		libvorbis-dev openal-soft-dev curl curl-dev freetype-dev zlib-dev \
		gmp-dev jsoncpp-dev postgresql-dev ca-certificates && \
	apk add --no-cache \
		--repository http://dl-cdn.alpinelinux.org/alpine/edge/community \
		leveldb-dev && \
	git config --global advice.detachedHead false && \
	git clone --depth=1 -b ${MINETEST_GAME_VERSION:-${MINETEST_RELEASE:-${MINETEST_RELEASE_DEFAULT}}} https://github.com/minetest/minetest_game.git ./games/minetest_game && \
	rm -fr ./games/minetest_game/.git

COPY startserver.sh /usr/local/sbin/startserver.sh

WORKDIR /usr/src/
RUN git clone --recursive https://github.com/jupp0r/prometheus-cpp/ && \
	mkdir prometheus-cpp/build && \
	cd prometheus-cpp/build && \
	cmake .. \
		-DCMAKE_INSTALL_PREFIX=/usr/local \
		-DCMAKE_BUILD_TYPE=Release \
		-DENABLE_TESTING=0 && \
	make -j2 && \
	make install && \
	echo "**** compile spatialindex ****" && \
	cd /usr/src/ && \
	git clone https://github.com/libspatialindex/libspatialindex/ && \
	cd libspatialindex && \
	cmake . \
		-DCMAKE_INSTALL_PREFIX=/usr/local && \
	make -j 2 && \
	make install && \
	echo "**** compile LuaJIT ****" && \
	cd /usr/src/ && \
	git clone -b v2.1 https://github.com/LuaJIT/LuaJIT.git && \
	cd LuaJIT && \
	make && \
	make install && \
	ln -sf luajit-2.1.0-beta3 /usr/local/bin/luajit && \
	chmod +x /usr/local/sbin/startserver.sh

WORKDIR /usr/src/minetest

RUN if [ -z ${MINETEST_RELEASE+x} ]; then \
		MINETEST_RELEASE=$(curl -sX GET "https://api.github.com/repos/minetest/minetest/releases/latest" \
		| awk '/tag_name/{print $4;exit}' FS='[""]'); \
	fi && \
	curl -s -o \
		/tmp/minetest-src.tar.gz -L \
		"https://github.com/minetest/minetest/archive/${MINETEST_RELEASE:-${MINETEST_RELEASE_DEFAULT}}.tar.gz" && \
	tar xf \
		/tmp/minetest-src.tar.gz --strip-components=1 && \
	cd build && \
	cmake .. \
		-DCMAKE_INSTALL_PREFIX=/usr/local \
		-DCMAKE_BUILD_TYPE=Release \
		-DBUILD_SERVER=TRUE \
		-DBUILD_CLIENT=FALSE \
		-DENABLE_PROMETHEUS=TRUE \
		-DBUILD_UNITTESTS=FALSE \
		-DENABLE_CURL=TRUE \
		-DENABLE_FREETYPE=TRUE \
		-DENABLE_GETTEXT=TRUE \
		-DENABLE_LUAJIT=TRUE \
		-DENABLE_REDIS=TRUE \
		-DENABLE_POSTGRESQL=TRUE \
		-DENABLE_LEVELDB=TRUE \
		-DENABLE_SYSTEM_GMP=TRUE \
		-DENABLE_SOUND=FALSE \
		-DRUN_IN_PLACE=FALSE \
		-DVERSION_EXTRA="${VERSION}_${BUILD_DATE}" \
		-DGETTEXT_LIBRARY=-lintl && \
	make -j2 && \
	make install && \
	cp -r locale /usr/local/share/minetest/
COPY game /usr/local/share/minetest/games/unej

FROM alpine:3.12

ARG PUID
ARG GUID
ARG BUILD_DATE
ARG VERSION

LABEL build_version="in-situ.iri-research.org version:- ${VERSION} Build-date:- ${BUILD_DATE}"
LABEL maintainer="BAM"


RUN apk add --no-cache sqlite-libs curl gmp libstdc++ libgcc libpq libintl bash \
		gettext rsync && \
	apk add --no-cache \
		--repository http://dl-cdn.alpinelinux.org/alpine/edge/community \
		leveldb && \
	addgroup -g ${GUID} minetest && \
	adduser -D -u ${PUID} -G minetest -h /var/lib/minetest minetest && \
	chown -R minetest:minetest /var/lib/minetest && \
	mkdir /var/log/minetest && \
	chown -R minetest:minetest /var/log/minetest

WORKDIR /var/lib/minetest

COPY --from=0 /usr/local/share/minetest /usr/local/share/minetest
COPY --from=0 /usr/local/bin /usr/local/bin
COPY --from=0 /usr/local/lib /usr/local/lib
COPY --from=0 /usr/local/share/doc/minetest /usr/local/share/doc/minetest
COPY --from=0 /usr/local/sbin/startserver.sh /usr/local/sbin/startserver.sh

USER minetest:minetest

EXPOSE 30000/udp 30000/tcp
VOLUME /etc/minetest
VOLUME /var/lib/minetest/worlds
VOLUME /var/log/minetest

CMD ["/usr/local/sbin/startserver.sh"]
