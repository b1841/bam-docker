# UNEJ/Minetest

[Minetest](http://www.minetest.net/) (server) is a near-infinite-world block sandbox game and a game engine, inspired by InfiniMiner, Minecraft, and the like.

[![minetest](https://raw.githubusercontent.com/linuxserver/beta-templates/master/lsiodev/img/minetest-icon.png)](http://www.minetest.net/)

## Usage

Here are some example snippets to help you get started creating a container.

### docker

```
docker create \
  --name=minetest \
  -e PUID=1000 \
  -e PGID=1000 \
  -e CLI_ARGS="--gameid minetest" `#optional` \
  -p 30000:30000/udp \
  -v <path to data>:/config/.minetest \
  -v <path to conf>:/etc/minetest
  -v <path to world data>:/var/lib/minetest/worlds
  -v <path to data>:/var/log/minetest
  --restart unless-stopped \
  registry.gitlab.com/iri-research-org/urbanum/minetest/docker-unej
```


### docker-compose

Compatible with docker-compose v3 schemas.
See in `compose` folder.

